FlatRent::Application.routes.draw do
  resources :photos

  resources :offers

  resources :events do
    collection do
         get :likeit
     end
  end

  authenticated do
    root :to => 'home#index', as: :authenticated
  end
  devise_scope :user do
    root to: "devise/sessions#new"
  end
  devise_for :users, :controllers => {:registrations => "registrations"}
  resources :users

  mount Commontator::Engine => '/commontator'
end