json.extract! @offer, :id, :title, :cost, :picture, :start_date, :end_date, :area, :rooms, :localization, :description, :created_at, :updated_at
